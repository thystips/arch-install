= Installation d'Arch Linux avec Luks, BTRFS et EFI
Antoine Thys
:doctype: book
:encoding: utf-8
:lang: fr
:toc: left
:numbered:
:sectanchors:
:sectlinks:

[preface]
== Pourquoi cette documentation ?

Je trouve que la documentation officielle d'Arch est très bien mais oblige un changement de page trop fréquent et à suivre différents liens en fonction de comment l'on veut installer son système exactement.

De plus cette documentation va me permettre d'avoir une version papier pour le jour où je passerais de Manjaro à Arch sur mon PC principal.

_À vrai dire il s'agit de la deuxième fois que j'écris cette documentation, la première fois je découvrais la documentation officielle et moment où je faisais les manipulations sur ma VM et ça a terminé avec un système qui n'avait pas le comportement désiré donc cette fois je vais prendre le temps de lire avant de faire._

IMPORTANT: Cette documentation commence à partir du boot de l'image d'installation (_cf._ https://wiki.archlinux.org/index.php/Installation_guide#Acquire_an_installation_image[documentation officielle]).

== Clavier

Dans un premier temps il faut utiliser le bon mappage pour le clavier, ça sera plus pratique quand même.

[source,bash]
----
root@archiso ~ # ls /usr/share/kbd/keymaps/**/*.map.gz #<1>
root@archiso ~ # loadkeys fr #<2>
----
<1> Liste des mappages disponibles
<2> Choix du clavier français standard

== Connexion réseau

[%hardbreaks]
Afin de pouvoir mener à bien l'installation une connexion à internet est nécessaire. Dans mon cas comme il s'agit d'une VM le dhcp récupère les informations réseaux et aucune configuration supplémentaire n'est requise.
Si vous avez besoin de vous connecter au wifi ou tout autre configuration réseau reportez-vous à la https://wiki.archlinux.org/index.php/Installation_guide#Connect_to_the_internet[documentation officielle].

=== SSH (optionnel)

Si vous préférez utiliser une connexion SSH pour effectuer l'installation voici la précédure à suivre:

[source,zsh]
----
root@archiso ~ # passwd #<1>
New password:
Retype new password:
passwd: password updated successfully
root@archiso ~ # systemctl start sshd #<2>
----
<1> Définition d'un mot de passe root
<2> Démarrage du serveur ssh

Il est maintenant possible de se connecter en ssh sur l'IP donnée avec la commande `ip a` afin de l'implifier la suite des opérations.

== Préparation du disque

Comme nous sommes sur un système UEFI il faut une table de partition GUID.

NOTE: Dans mon cas le disque utilisé est le `/dev/nvme0n1`.

.Table de partition
[source,zsh]
----
root@archiso ~ # parted /dev/nvme0n1
(parted) mklabel gpt
----

On va ensuite crée une partition pour le démarrage EFI et une autre qui sera utilisé plus tard pour le système et la swap.

.Création des partitions
[source,zsh]
----
(parted) mkpart "EFI system partition" fat32 1MiB 261MiB #<1>
(parted) set 1 esp on #<1>
(parted) mkpart "lvm" btrfs 261MiB 100% #<2>
----
<1> Partition EFI
<2> Partition principale (j'ai indiqué BTRFS comme système de fichiers mais cela n'a aucune importance)

== Préparation des partitions

Dans un premier temps il faut chiffrer la partition principale

.Chiffrement la deuxième partition
[source,zsh]
----
root@archiso ~ # cryptsetup luksFormat /dev/nvme0n1p2

WARNING!
========
This will overwrite data on /dev/nvme0n1p2 irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for /dev/nvme0n1p2:
Verify passphrase:
WARNING: Locking directory /run/cryptsetup is missing!
cryptsetup luksFormat /dev/nvme0n1p2  35.41s user 5.05s system 158% cpu 25.468 total
----

.Ouverture du conteneur chiffré
[source,zsh]
----
root@archiso ~ # cryptsetup open /dev/nvme0n1p2 cryptlvm
Enter passphrase for /dev/nvme0n1p2:
cryptsetup open /dev/nvme0n1p2 cryptlvm  6.16s user 0.36s system 119% cpu 5.457 total
----

.Formattage de la partition EFI
[source,zsh]
----
root@archiso ~ # mkfs.fat -F32 /dev/nvme0n1p1
mkfs.fat 4.1 (2017-01-24)
----

== Ajout de LVM dans la partition chiffrée

.Création du volume physique sur le conteneur LUKS
[source,zsh]
----
root@archiso ~ # pvcreate /dev/mapper/cryptlvm
  Physical volume "/dev/mapper/cryptlvm" successfully created.
----

.Création du groupe de volume
[source,zsh]
----
root@archiso ~ # vgcreate secure /dev/mapper/cryptlvm
  Volume group "secure" successfully created
----

.Création des volumes logiques
[source,zsh]
----
root@archiso ~ # lvcreate -L 3G secure -n swap #<1>
  Logical volume "swap" created.
root@archiso ~ # lvcreate -l 100%FREE secure -n system #<2>
  Logical volume "system" created.
----
<1> Volume pour la swap, libre à vous de choisir la taille de cette partition
<2> Le reste de la place sert pour le système

== Création du système de fichier et montage des partitions

=== BTRFS et sous volumes

.Création du BTRFS
[source,zsh]
----
root@archiso ~ # mkfs.btrfs /dev/mapper/secure-system
----

.Création des sous volumes BTRFS
[source,zsh]
----
root@archiso ~ # mount /dev/mapper/secure-system /mnt

root@archiso ~ # btrfs subvolume create /mnt/@
Create subvolume '/mnt/@'

root@archiso ~ # btrfs subvolume create /mnt/@home
Create subvolume '/mnt/@home'

root@archiso ~ # btrfs subvolume create /mnt/@snapshots
Create subvolume '/mnt/@snapshots'

root@archiso ~ # mkdir /mnt/@/var
root@archiso ~ # btrfs subvolume create /mnt/@/var/log
Create subvolume '/mnt/@/var/log'
----

.Démontage de BTRFS
[source,zsh]
----
root@archiso ~ # umount /mnt
----

=== Montage des partitions

.Montage du sous volume principal
[source,zsh]
----
root@archiso ~ # mount -o subvol=@ /dev/mapper/secure-system /mnt
----

.Création des points de montage
[source,zsh]
----
root@archiso ~ # mkdir -p /mnt/home /mnt/boot /mnt/var/log /mnt/.snapshots
----

.Montage des sous volumes
[source,zsh]
----
root@archiso ~ # mount -o subvol=@home /dev/mapper/secure-system /mnt/home
root@archiso ~ # mount -o subvol=@snapshots /dev/mapper/secure-system /mnt/.snapshots
root@archiso ~ # mount -o subvol=@/var/log /dev/mapper/secure-system /mnt/var/log
----

.Montage de la partition de EFI
[source,zsh]
----
root@archiso ~ # mount /dev/nvme0n1p1 /mnt/boot
----

=== Swap

Avant d'oublier il faut créer la swap et l'activer

.Création de la swap
[source,zsh]
----
root@archiso ~ # mkswap /dev/mapper/secure-swap
Setting up swapspace version 1, size = 3 GiB (3221221376 bytes)
no label, UUID=5825294c-ea77-4bba-9712-bab152b2037f
----

.Activation de la swap
[source,zsh]
----
root@archiso ~ # swapon /dev/mapper/secure-swap
----

== Installation d'Arch

=== Choix des miroirs

Afin de récupérer les paquets pour l'installation il est nécessaire d'avoir des miroirs stables.

.Choix des miroirs
[source,zsh]
----
root@archiso ~ # reflector --country France --country Germany --latest 10 --sort rate --save /etc/pacman.d/mirrorlist #<1>
----
<1> Avec cette commande nous prenons les 10 derniers miroirs de France et d'Allemagne qui se sont synchronisés en les triant par performance

=== Installation des paquets essentiels

La documentation offcielle recommande d'installer les paquets `base, linux et linux-firmware` et d'y ajouter des paquets spécifiques aux caractéristiques de l'installation plus d'autres paquets nécessaires comme un éditeur de fichier.

NOTE: Comme mon système de test est une VM je vais rajouter le paquet `open-vm-tools` qui ne sera pas indiqué dans <<installation-du-systeme>>

[[installation-du-systeme, Installation du système]]
.Installation du système
[source,zsh]
----
root@archiso ~ # pacstrap /mnt base base-devel linux linux-firmware git vim nano ntp bluez-utils blueman networkmanager btrfs-progs dosfstools lvm2 man-db man-pages texinfo wget zsh os-prober openssh
----

== Configuration du système

=== Fstab

Nous commençons par la génération du fichier `fstab` qui permet d'informer le systèmes des partions et de leur point de montage associé.

.Génération du fichier fstab
[source,zsh]
----
root@archiso ~ # genfstab -U /mnt >> /mnt/etc/fstab
----

=== Chroot

Il est maintenant temps de rentrer dans notre nouveau système.

.Chroot
[source,zsh]
----
root@archiso ~ # arch-chroot /mnt
----

=== Zone temporelle

NOTE: Comme je me trouve en France je vais sélectionner Paris comme zone temporelle.

.Définition de la zone de temps
[source,bash]
----
[root@archiso /]# ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime
----

.Mise à jour de l'heure du système
[source,bash]
----
[root@archiso /]# hwclock --systohc
----

=== Localisation

Il faut maintenant éditer le fichier `/etc/locale.gen` et décommenter les locales dont nous avons besoin dans mon cas il s'agit de `fr_FR.UTF-8` et `en_US.UTF-8`, ensuite il suffit de lancer la génération avec la commande suite.

.Génération des locales
[source,bash]
----
[root@archiso /]# locale-gen
Generating locales...
  en_US.UTF-8... done
  fr_FR.UTF-8... done
Generation complete.
----

Il faut ensuite choisir la langue du système, personnellement j'hésite à chaque fois parce qu'avoir mon système en anglais me rapproche de la configuration de la majeure partie des serveurs en production mais d'un autre côté j'aime bien avoir la date en français.

Une fois la décision prise il faut créer le fichier `/etc/locale.conf`.

.`/etc/locale.conf`
[source,conf]
----
LANG=en_US.UTF-8
----

On va ensuite définir l'agencement du clavier de façon permanente, il suffit d'utiliser la même valeur qu'à l'étape du choix du mappage du <<Clavier>>

.`/etc/vconsole.conf`
[source,conf]
----
KEYMAP=fr
----

=== Configuration réseau

CAUTION: Pour cet exemple le nom de la machine sera `unicorn`, libre à vous de remplacer ce mot par le nom d'hôte choisi.

.Définition du nom d'hôte
[source,bash]
----
[root@archiso /]# echo "unicorn" > /etc/hostname
----

.`/etc/hosts`
[source,hosts]
----
127.0.0.1   localhost
::1         localhost
127.0.1.1   unicorn.localdomain unicorn
----

NOTE: Dans le cas d'une IP fixe `127.0.1.1` peut être remplacé par cette IP.

Pour avoir du réseau au redémarrage il peut être utile de lancer `Network Manager` au démarrage.

.Network Manager
[source,bash]
----
[root@archiso /]# systemctl enable NetworkManager
----

== Configuration du chargeur de démarrage

=== Mise à jour du microcode

Si vous avez un CPU Intel :
[source,bash]
----
[root@archiso /]# pacman -S intel-ucode
----

Et pour le CPU AMD :
[source,bash]
----
[root@archiso /]# pacman -S amd-ucode
----

=== Installation du chargeur de démarrage

.Installation de systemd-boot
[source,bash]
----
[root@archiso /]# bootctl --esp-path=/boot install
Created "/boot/EFI".
Created "/boot/EFI/systemd".
Created "/boot/EFI/BOOT".
Created "/boot/loader".
Created "/boot/loader/entries".
Created "/boot/EFI/Linux".
Copied "/usr/lib/systemd/boot/efi/systemd-bootx64.efi" to "/boot/EFI/systemd/systemd-bootx64.efi".
Copied "/usr/lib/systemd/boot/efi/systemd-bootx64.efi" to "/boot/EFI/BOOT/BOOTX64.EFI".
Created "/boot/16da10d4ea33428f8cb121efde519236".
Random seed file /boot/loader/random-seed successfully written (512 bytes).
Not installing system token, since we are running in a virtualized environment.
Created EFI boot entry "Linux Boot Manager".
----

=== Création d'une entrée

Le fichier ci-dessous n'existe pas il faut donc le créé, il faut également modifier `FS_UUID` par l'UUID de votre partition LUKS.

WARNING: L'option `discard` réduit légèrement la sécurité du système mais permet d'allonger la durée de vie d'un SSD, à vous de choisir.

.`/boot/loader/entries/arch.conf`
[source,conf]
----
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img     # SEULEMENT POUR LES CPU Intel !!
initrd /amd-ucode.img       # SEULEMENT POUR LES CPU AMD !!
initrd /initramfs-linux.img
options luks.uuid=FS_UUID root=/dev/mapper/secure-system rootflags=subvol=@ rd.luks.options=discard
----

=== Configuration du chargeur de démarrage

Il faut ensuite indiqué au chargeur de démarrage d'utiliser notre configuration.

.`/boot/loader/loader.conf`
[source,conf]
----
default arch    # Le nom du fichier précédent sans l'extension .conf
timeout 2
editor  0
----

=== Ramdisk

Il faut effectuer certaines modifications afin de permettre au système d'avoir les composant nécessaire pour démarrer sur une partition LVM chiffrée.

Il faut donc modifier la ligne des `HOOKS` dans le fichier `/etc/mkinitcpio.conf`.

.`/etc/mkinitcpio.conf`
[source,conf]
----
HOOKS=(base systemd autodetect modconf keyboard sd-vconsole block sd-encrypt sd-lvm2 filesystems fsck)
----

.Génération de l'image
[source,bash]
----
[root@archiso /]# mkinitcpio -p linux
----

=== Mot de passe Root

Pour éviter de commettre la même erreur que moi et devoir remonter les partitions pour recommencer un chroot il faut définir un mot de passe root !

.Définition du mot de passe root
[source,bash]
----
[root@archiso /]# passwd
----

== Redémarrage

Il est maintenant temps de redémarrer proprement, c'est-à-dire dans un premier temps sortir du chroot, démonter les partition et redémarrer.

.Reboot
[source,zsh]
----
[root@archiso /]# exit
root@archiso ~ # umount -R /mnt
root@archiso ~ # reboot
----

[CAUTION]
====
Il est courant dans les installations avec `LUKS` et `Grub` que le clavier soit en `QWERTY` mais avec ce type d'installation le mappage clavier utilisé pour le mot de passe LUKS est celui défini dans le fichier `/etc/vconsole.conf`.
====

== Personnalisation du système

Il peut être pratique d'avoir une interface graphique, un utilisateur ainsi que d'autres choses sur ce nouveau système.

NOTE: Mes choix en matière de personnalisation du système mes choix peuvent être particuliers.

=== Utilisateur

==== Création d'un utilisateur

[source,bash]
----
[root@unicorn ~]# useradd -m -s /bin/zsh unicorn #<1>
[root@unicorn ~]# passwd unicorn #<2>
New password:
Retype new password:
passwd: password updated successfully
----
<1> Création de l'utilisateur
<2> Changement du mot de passe

Nous avons maintenant un nouvel utilisateur appelé `unicorn`.

NOTE: J'utilise `ZSH` au lieu de `BASH` par préférence personnelle, pour utiliser `BASH` il suffit de ne pas ajouter `-s /bin/zsh`.

==== Sudo

Comme les bonnes pratiques veulent que l'ont n'utilise pas l'utilisateur `root` il faut donc que notre nouvel utilisateur puisse utiliser la commande `sudo`.

.Installation de sudo
[source,bash]
----
[root@unicorn ~]# pacman -S sudo
----

Il se peut que le paquet soit déjà installé pas besoin de le réinstaller.

Il faut maintenant donner des droits à notre utilisateur :

.`/etc/sudoers.d/unicorn`
[source,bash]
----
unicorn		ALL=(ALL:ALL)	ALL <1>
----
<1> Cette ligne donne tous les droits à notre utilisateur

NOTE: Remplacez bien-sûr `unicorn` par le nom de votre utilisateur.

=== Interface graphique

Pour avoir une interface graphique complète, il faut installer plusieurs composants.

==== Serveur graphique

Dans un premier temps impossible d'afficher quoique ce soit sans un composant adéquat qui est le serveur graphique. J'ai choisi d'utiliser le plus courant `Xorg`.

Comme je ne cherche pas un système minimal je vais installer le groupe https://www.archlinux.org/groups/x86_64/xorg/[Xorg] complet pour avoir toutes les dépendances utiles.

.Installation de `Xorg`
[source,bash]
----
[root@unicorn ~]# pacman -S xorg
----

==== Pilotes graphiques spécifiques

Des pilotes graphiques supplémentaires peuvent être nécessaire spécialement avec les cartes `AMD` et `Nvidia`.

Vous pouvez trouver des informations supplémentaires si dessous :

* https://wiki.archlinux.org/index.php/Xorg#AMD[AMD]
* https://wiki.archlinux.org/index.php/Intel[Intel]
* https://wiki.archlinux.org/index.php/NVIDIA[NVIDIA]

==== Gestionnaire de connexion

Les gestionnaires de connexion permet de choisir l'environnement graphique à utiliser et d'ouvrir une session.

Pour cette installation j'ai choisi `SDDM` c'est celui qui est utilisé avec l'environnement `KDE`.

.Installation de `sddm`
[source,bash]
----
[root@unicorn ~]# pacman -S sddm sddm-kdm
----

.Démarrage automatique de `sddm`
[source,bash]
----
[root@unicorn ~]# systemctl enable sddm
----

Un problème courant avec SDDM et que Xorg n'a que le clavier `us` chargé par défaut, il faut donc indiqué à Xorg quel clavier utiliser.

.Problème de clavier US
[source,bash]
----
[root@unicorn ~]# localectl set-x11-keymap fr
----

Donc au prochain démarrage `SDDM` sera lancé.

Le verrouillage du pavé numérique est désactivé par défaut, pour l'activer il faut éditer le fichier `/usr/lib/sddm/sddm.conf.d/default.conf`.

.`/usr/lib/sddm/sddm.conf.d/default.conf`
[source,conf]
----
[General]
...
Numlock=on
----

==== Environnement graphique

Ne nom de ce chapitre est un abus de language car justement je ne compte pas utiliser d'environnement de bureau mais un "gestionnaire de fenêtres par pavage" plus connu sous le nom de `Tiling window manager`.

IMPORTANT: Si vous ne connaissez pas `i3-gaps` ou n'avez pas l'habitude des `Tiling window manager` je vous conseille de regarder comme installer https://wiki.archlinux.org/index.php/GNOME[Gnome], https://wiki.archlinux.org/index.php/KDE[KDE], https://wiki.archlinux.org/index.php/LXDE[LXDE] ou https://wiki.archlinux.org/index.php/Xfce[Xfce].

.Installation de i3 et certaines dépendances
[source,bash]
----
[root@unicorn ~]# pacman -S i3-gaps i3status py3status conky nitrogen picom rofi rxvt-unicode gtk2-perl dunst pcmanfm arandr tmux #<1>
----
<1> L'installation de tmux n'est pas nécessaire mais comme je l'utilise régulièrement je l'installe le plus tôt possible.

En plus de i3 j'ai ajouté certaines dépendances comme un compositeur et un gestionnaire de fond d'écran, les configurations associées ainsi que les dépendances supplémentaires seront dans les <<Dotfiles, dotfiles>>.

=== Personnalisation du terminal

Maintenant nous allons rendre notre terminal un peu plus pratique.

Mais avant tout il faut faire une petit redémarrage afin de vérifier que tout ce lance correctement (libre à vous de lancer `i3` ou `SDDM` manuellement à la place).

.Redémarrage
[source,bash]
----
[root@unicorn ~]# reboot
----

Il est maintenant possible de se connecter.

Dans un premier temps il faut se connecter à l'utilisateur nouvellement créé et d'ouvrir le terminal `rxvt`, pour `i3` le raccourci par défaut devrait être `Win/Alt + Entrée` cela dépend de la touche choisie.

NOTE: Le terminal ne pas être très joli (du moins à mon goût) mais ce problème peut-être résolu par le fichier `.Xresources` des <<Dotfiles, dotfiles>>.

Ensuite `ZSH` va demander ce que vous voulez faire, je vous conseille d'appuyer sur la touche `q` afin de ne pas effectuer de configuration qui sera de toute façon écrasée par la suite.

==== Oh My Zsh

Je recommande l'utilisation de `Oh My Zsh` pour la personnalisation de `zsh`, son installation est particulièrement simple.

.Oh My Zsh
[source,zsh]
----
unicorn% sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
----

==== Police

Par contre pour le thème que j'utilise il faut changer la police utilisée, j'ai choisi MesloLGS de https://www.nerdfonts.com/font-downloads[NerdFont] mais tout autre police de https://www.nerdfonts.com/font-downloads[NerdFont] fonctionnera aussi.

.Gestion des fichiers zip
[source,zsh]
----
➜  ~ sudo pacman -S zip unzip wget
----

.Installation de la police
[source,zsh]
----
➜  ~ wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip -O ~/meslo.zip
➜  ~ unzip meslo.zip -d ~/meslo && rm -f meslo.zip
➜  ~ find ~/meslo/ -iname "*Windows*" -exec rm {} \; #<1>
➜  ~ sudo mv meslo /usr/share/fonts
➜  ~ fc-cache -v #<2>
----
<1> Suppression des polices pour windows
<2> Mise à jour du cache des polices

==== Dotfiles

Dans ce dépôt il y a un sous module git avec mes dotfiles.

NOTE: Ce sous module utilisant le lien ssh du dépôt associé si vous n'êtes pas membre du projet sur gitlab vous ne pourrez pas utiliser le sous module. Dans ce cas vous pouvant cloner le dépôt dotfiles à part.

La commande suivante permet de récupérer le sous module depuis le dépôt Arch Install.

.dotfiles depuis Arch Install
[source,bash]
----
$ git submodule update --remote --init
----

====
Sinon voici le lien du dépôt https://gitlab.com/antoine33520/dotfiles[dotfiles].
====

.Clonage du dépôt
[source,zsh]
----
➜  ~ git clone https://gitlab.com/antoine33520/dotfiles.git
----

TIP: Pour l'utilisation de ces configurations lisez la documentation ;-)

=== AUR

Les `AUR` sont les paquets mis à disposition par la communauté et qui ne peuvent pas être installés via `pacman`, si vous voulez y avoir accès il faut donc un gestionnaire de paquet adéquat.

J'ai choisi d'installer `yay` pour remplir cette fonction.

[CAUTION]
====
L'utilitaire `yay` ne doit jamais être utilisé avec les droits root.
====

.Installation de `yay`
[source,zsh]
----
➜  ~ git clone https://aur.archlinux.org/yay.git
➜  ~ cd yay
➜  yay git:(master) makepkg -si
----

TIP: `yay` permet également d'installer les paquets fournis par les dépôts officiels comme le fait `pacman`.

NOTE: Les arguments pour `yay` sont les mêmes qu'avec `pacman`.

=== Son

Ça peut être utile d'avoir du son sur sa machine.

Les 2 serveurs de son les plus utilisés sont `Alsa` et `Pulseaudio`, j'ai choisi le deuxième par préférence et pour avoir mon equalizer.

.Installation de Pulseaudio
[source,zsh]
----
➜  ~ sudo pacman -S pulseaudio pulseaudio-equalizer pulseaudio-bluetooth pulseaudio-jack pulseaudio-equalizer-ladspa pulseaudio-zeroconf pulseaudio-alsa #<1>
----

En fonction de ce que vous voulez faire tous les paquets ne sont pas nécessaire.

== Conclusion

Il me semble que j'ai terminé pour ce qui est des installations.

Je vais quand même remettre le lien vers mes https://gitlab.com/antoine33520/dotfiles[dotfiles] pour donner des idées de configurations.
